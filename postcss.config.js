module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-initial': {},
    'postcss-cssnext': {
      warnForDuplicates: false,
    },
    'cssnano': {},
  },
};
