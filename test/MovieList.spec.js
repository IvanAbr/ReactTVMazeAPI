import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';

import MovieList from './../src/components/MovieList/MovieList';

describe('', function () {
   it('should have props for movies', function () {
    const wrapper = shallow();
    expect(wrapper.props().movies).to.be.defined;
  });
});