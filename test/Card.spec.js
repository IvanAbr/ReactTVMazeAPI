import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';

import Card from './../src/components/Card/Card';

describe('', function () {
   it('should have props for title', function () {
    const wrapper = shallow();
    expect(wrapper.props().title).to.be.defined;
  });

  it('should have props for img', function () {
    const wrapper = shallow();
    expect(wrapper.props().img).to.be.defined;
  });

  it('should have props for year', function () {
    const wrapper = shallow();
    expect(wrapper.props().year).to.be.defined;
  });

  it('should have props for type', function () {
    const wrapper = shallow();
    expect(wrapper.props().type).to.be.defined;
  });

  it('should have props for rating', function () {
    const wrapper = shallow();
    expect(wrapper.props().rating).to.be.defined;
  });
});