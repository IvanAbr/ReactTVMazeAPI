import { combineReducers } from 'redux'

import  movies  from './movies';
import setSort from './setSort';

const reducer = combineReducers({
    movies,
    setSort
})

export default reducer;