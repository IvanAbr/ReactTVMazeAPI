import {GET_MOVIE} from '../const/movies';

const movies = (state = [], action) => {
    switch (action.type) {
        case GET_MOVIE:
            return action.movies;
        
        default: return state;
    }
}
export default movies;
