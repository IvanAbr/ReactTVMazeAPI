import 'normalize.scss/normalize.scss';
import './styles.scss';
import App from './App.jsx';
import {Provider} from 'react-redux';
import ReactDOM from 'react-dom';
import React from 'react';
import { createStore,applyMiddleware } from 'redux';
import { getMovie } from './actions/movies'
import  reducer  from './reducers/index'

const store = createStore(reducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, document.getElementById('root')
);


