import React from 'react';
import MovieList from './components/MovieList/MovieList.jsx';
import Movie from './components/Movie/Movie.jsx';
import Search from './components/Search/Search.jsx';
import NotFound from './components/NotFound/NotFound.jsx';
import NotFilms from './components/NotFilms/NotFilms.jsx';

import { BrowserRouter as Router, Route, NavLink, Switch } from 'react-router-dom';
import {connect} from 'react-redux';

class App extends React.Component {
constructor() {
  super();
  this.handleSort = this.handleSort.bind(this);
}

handleSort(filter) {
  this.props.onSetSort(filter);
}

  render() {
    return (
    <Router>
      <div>
        <Route exact path='/' render={() => <Search  onSort={this.handleSort} movies={this.props.movies}  /> } />
        <Route  path='/search/:find' render={() => <Search  onSort={this.handleSort} movies={this.props.movies}  /> } />
        <Route exact path='/search/' render={() => <Search  onSort={this.handleSort} movies={this.props.movies}  /> } />
        <Switch>
          <Route exact path='/search/' component={NotFilms} />
          <Route exact path='/' component={NotFilms} />
          <Route path='/search/:find' render={(props) => <MovieList  movies={this.props.movies} name={props}/> } />
          <Route path='/film/:name' render={props => <Movie data={this.props.movies} {...props} />} />
          <Route path='*' component={NotFound} />
        </Switch>
      </div>
    </Router>
    );
  }
}
const getVisibleTodos = (movies, filter) => {
    switch (filter) {
      case 'SHOW_DATA':
        return movies.sort((a,b) => (b.show.premiered ? b.show.premiered.replace( /-/g, "" ) : '2000') - (a.show.premiered ? a.show.premiered.replace( /-/g, "" ) : '2000'))
      case 'SHOW_RATING':
        return movies.sort((a,b) => b.show.rating.average-a.show.rating.average)
      default:
        throw new Error('Unknown filter: ' + filter)
    }
  }
export default connect(
  state => ({
      movies: getVisibleTodos(state.movies,state.setSort) 
  }),
  dispatch => ({
    onSetSort: (filter) => {
      dispatch({ type:'SET_SORT', filter:filter })
    }
  })
)(App)


