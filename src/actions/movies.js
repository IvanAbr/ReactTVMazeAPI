import {GET_MOVIE} from '../const/movies';

export const getMovie = (movies) => ({
    type: GET_MOVIE,
    movies
});
