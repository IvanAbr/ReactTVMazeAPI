import React from "react";
import "./Movie.scss";
import { BrowserHistory } from 'react-router-dom';
import Footer from './../Footer/Footer.jsx';
import { BrowserRouter as Router, Route, NavLink, Switch } from 'react-router-dom';
import history from './../../../src/history'

  function Movie( {match,data} ) {
    const movie = data.find(movie => movie.show.name === match.params.name)
    function createMarkup() {
        return {__html: movie.show.summary};
      }
    return (
        <div className='movie'>
            <div className='movie_container'>
                <div className='movie_top'>
                    <span className='netflixroulette'>netflixroulette</span>
                    <button onClick={()=>history.goBack()} className='link'>Search</button>
                </div>
                <div key={movie.show.id} className='movie_item'>
                    <div className="movie_item_image">
                        <img className='movie_img' src={movie.show.image ? movie.show.image.medium : 'http://www.bugaga.ru/uploads/posts/2017-03/1489052030_kotik-hosiko-12.jpg' } 
                        onError={(ev)=>ev.target.src='http://www.bugaga.ru/uploads/posts/2017-03/1489052030_kotik-hosiko-12.jpg'} alt=""/>
                    </div>
                    <div className="movie_item_info">
                        <div>
                            <span className="movie_item_title">{movie.show.name}</span>
                            <span className="movie_item_rating">{movie.show.rating.average ? movie.show.rating.average : '4.2'}</span>
                        </div>
                        <div className='movie_list_category'>
                            {(movie.show.genres.map((item,index)=><span key={index} className='movie_category'>{item}</span>))}
                        </div>
                        <div>
                            <span className="movie_item_realese">{movie.show.premiered}</span>
                            <span className="movie_item_realese">{movie.show.runtime+' min'}</span>
                        </div>
                            <div className="movie_item_summary" dangerouslySetInnerHTML={createMarkup()} ></div>
                    </div>
                </div>
                <Footer/>
            </div>
        </div>
       );
}

export default Movie