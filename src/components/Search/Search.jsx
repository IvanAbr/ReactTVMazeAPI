import "./Search.scss";
import React from "react";
import {connect} from 'react-redux';
import {getMovie} from '../../actions/movies';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            sort:true
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleClickSort = this.handleClickSort.bind(this);
    }

    handleChange(event) {
        let name = event.target.value;
        this.setState({ name });
    }

    handleClickSort(event) {
        this.setState({
           sort: !this.state.sort,
       })
       let sort = this.state.sort ? 'SHOW_RATING' : 'SHOW_DATA'
       this.props.onSort(sort)
    }

    render() {
       return (
    <div>
        <div className="container">      
            <div className='search_header'>
                <h1 className='search_title' >netflixroulette</h1>
            <div> 
                <label className='search_label' >FIND YOUR MOVIE</label>
                <input 
                value={this.state.name} 
                type="text"
                onChange={this.handleChange} />
            <div className="search_bottom">
                <div className="search">
                    <Link to={`/search/${this.state.name}`}>
                        <button className='search_btn'>SEARCH</button>
                    </Link>
                </div>
          </div>
          </div>
          </div>  
        </div>
        {this.props.movies.length ? 
        <div className='result'>
            <span>Sort by</span>
            { this.state.sort ? 
                <div className='result_btn'>
                    <a href='#' className='result_btn--active' > 
                        release date
                    </a>
                    <a href='#' onClick={this.handleClickSort} >
                        rating
                    </a>
                </div>
                :
                <div className='result_btn'>
                    <a href='#'  onClick={this.handleClickSort}> 
                        release date
                    </a>
                    <a href='#' className='result_btn--active'>
                        rating
                    </a>
                </div>
            }
        </div> 
        :
        <div></div> }
    </div>
       );
    }   
}

export default Search