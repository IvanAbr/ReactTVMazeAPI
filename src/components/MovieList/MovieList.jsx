import "./MovieList.scss";
import React from "react";
import Card from './../Card/Card.jsx';
import Search from './../Search/Search.jsx';
import Results from './../Results/Results.jsx';
import Footer from './../Footer/Footer.jsx';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import NotFilms from './../NotFilms/NotFilms.jsx';
import {connect} from 'react-redux';
import {getMovie} from '../../actions/movies';

class MovieList extends React.Component {
  constructor(props) {
  super(props)
  }
  componentWillReceiveProps(props) {
    if (props.name.match.params.find !== this.props.name.match.params.find) {
    const {getMovie} = this.props;
    fetch(`http://api.tvmaze.com/search/shows?q=${props.name.match.params.find}`)
    .then(response => {
      if (response.status !== 200) {
      console.log('Looks like there was a problem. Status Code: ' + response.status)
  }        
      return response.json()      
  })
  .then(movies => {
      if (movies instanceof Array) { 
        getMovie(movies)
      }

      else if (typeof movies.errorcode === 'number') {
          console.log('Oops, we don`t have any films with this title')
          let item = []
          getMovie(item)
      }

      else {
          let title = [].concat(movies);
          getMovie(title)
      }
  })   
  .catch(err =>
      console.log('Fetch Error ', err));
  }
}
  componentDidMount() {
    const {getMovie} = this.props;
        fetch(`http://api.tvmaze.com/search/shows?q=${this.props.name.match.params.find}`)
        .then(response => {
            if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status)
        }        
            return response.json()      
        })
        .then(movies => {
            if (movies instanceof Array) { 
              getMovie(movies)
            }
    
            else if (typeof movies.errorcode === 'number') {
                console.log('Oops, we don`t have any films with this title')
                let item = []
                getMovie(item)
            }
    
            else {
                let title = [].concat(movies);
                getMovie(title)
            }
        })   
        .catch(err =>
            console.log('Fetch Error ', err)); 
      }
  
      render() {
      return (
        <div>
          <div>{this.props.movies.length ? <div className='movies_found'>{this.props.movies.length+' movies found'}</div>  : <NotFilms/>}</div>
            <div className='movie-list_container'>
            <div className="movie-list">
              {this.props.movies.map((item,i) =>  
                <Card 
                  key={i}
                  item={item}/>)}
            </div>
            </div>
            { this.props.movies.length ? <Footer /> : <div></div> }
        </div>
      );
    }
    };

export default connect(
  state => ({
    title: state.setSearch
  }),
  dispatch => ({
    getMovie: (movies) => {
      dispatch({ type: 'GET_MOVIE', movies: movies })
    }
  })
)(MovieList)
