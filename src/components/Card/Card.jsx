import "./Card.scss";
import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import Movie from './../Movie/Movie.jsx';

function Card(props) {
  return (
    <div className="movie-list_item">
      <div className="card_header">
        <Link className='card_link' to={`/film/${props.item.show.name}`}>
          <img className='card_img' src={props.item.show.image ? props.item.show.image.medium : 'http://www.bugaga.ru/uploads/posts/2017-03/1489051984_kotik-hosiko-3.jpg' } 
          onError={(e)=>e.target.src='http://www.bugaga.ru/uploads/posts/2017-03/1489051984_kotik-hosiko-3.jpg'}/>
        </Link>
      </div>
      <div className="card_bottom">
        <span className='card_title'>{props.item.show.name}</span>
        <span className='card_year'>{props.item.show.premiered ? props.item.show.premiered.slice(0,4) : '2005'}</span>
      </div>
      <div className="card_bottom">
         <span className='card_category'>{props.item.show.genres[0] ? props.item.show.genres[0] : 'Films'}</span>
      </div>
    </div>
  );
};

export default Card;
