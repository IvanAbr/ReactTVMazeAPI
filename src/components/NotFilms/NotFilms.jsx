import React from "react";
import "./NotFilms.scss";
import Footer from './../Footer/Footer.jsx';
function NotFilms() {
    return (
      <div>
          <h2 className="not_films">No films found</h2>
          <Footer/>
      </div>
    );
  };
  
  export default NotFilms;