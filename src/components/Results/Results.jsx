import "./Results.scss";
import React from "react";
import { connect } from 'react-redux';

function Results(props) {
    return (
        <div className='result'>
            Sort by
            {' '}
            
            <a href='#' onClick={props.onSortby(props.movies)}> 
                release date
            </a>
            {' '}
            <a href='#' onClick={props.onSortby(props.movies)}>
                rating
            </a>
        </div>
        );
    }


export default Results