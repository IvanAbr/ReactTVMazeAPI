import React from "react";
import "./NotFound.scss";

function NotFound() {
    return (
      <div className="not-found">
          <h2>Не найдено :(</h2>
      </div>
    );
  };
  
  export default NotFound;